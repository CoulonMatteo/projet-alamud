# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import TakeEvent, TakeBuyEvent

class TakeAction(Action2):
    EVENT = TakeEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "take"

class TakeBuyAction(Action3):
    EVENT = TakeBuyEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_take"
    ACTION = "buy"