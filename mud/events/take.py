# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class TakeEvent(Event2):
    NAME = "take"

    def perform(self):
        if not self.object.has_prop("takable"):
            self.add_prop("object-not-takable")
            return self.take_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()
        self.object.move_to(self.actor)
        self.inform("take")

    def take_failed(self):
        self.fail()
        self.inform("take.failed")

class TakeBuyEvent(Event3):
    NAME = "buy"

    def perform(self):
        if not self.object.has_prop("buyable"):
            self.add_prop("object-not-buyable")
            return self.buy_failed()
        if not self.object2.has_prop("money"):
            self.fail()
            self.add_prop("object-not-money")
            return self.buy.failed()
        self.object.move_to(self.actor)
        self.object2.move_to(None)
        self.inform("buy")
    
    def buy_failed(self):
        self.fail()
        self.inform("buy.failed")